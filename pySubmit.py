#!/usr/bin/env python3
'''
Prepares arXiv or journal submission: copies all relevant files (tex, bbl, figures, etc.).
Can remove comments, double spaces and double carriage returns from tex file.
Places figures in user-specified directory. Can check for empty lines within equations.
By default assumes pdflatex compilation. Creates a tar.gz file to be submitted.
Autor: Vicent Mateu (2021)
'''

#pySubmit.py - python3 script to prepare arXiv or journal submissions
#
#The script creates a new copy of the tex file, removing all comments. If specified, by
#the user it will also delete a) double whitespaces, trailing whitespaces or indentation,
#b) double carriage returns. It will try to copy figures in pdf format, but if not
#found, it will look for the following formats (in this order): eps, png, jpg, jpeg,
#jbig2, jp2 The script creates a user-specified directory and places all figures in
#the same directory as the tex and bbl files (plus a number of other user-specified
#files), while the tex file is modified accordingly (this is necessary to submit e.g.
#to APS journals). All files are then compressed into a tar.gz, ready for submission
#to arXiv or any journal. By default the script prepares the submission files to be
#compiled with pdflatex.
#
#Get help:
#
#./pySubmit.py -h
#./pySubmit.py --help
#
#Copyright: 2021 Vicent Mateu
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of this
#software and associated documentation files (the "Software"), to deal in the Software
#without restriction, including without limitation the rights to use, copy, modify,
#merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
#permit persons to whom the Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or
#substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
#BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
#DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys, os, tarfile, re, pathlib, argparse
from shutil import copy2
import numpy as np

formatList = ['.pdf', '.eps', '.png', '.jpg', '.jpeg', '.jbig2', '.jp2']
eqList = ['equation', 'align', 'eqnarray']

def find_macros(latex_content):
    # This pattern now captures \newcommand{\macro} or \newcommand{\macro}[n]
    pattern = r'\\newcommand{\\(\w+)}(?:\[(\d+)\])?{(.+?)}'
    macros = {}
    
    # Find all matches and store macros with their definitions and argument count (if any)
    for match in re.findall(pattern, latex_content):
        macro, arg_count, definition = match
        macros[macro] = {
            'definition': definition,
            'arg_count': int(arg_count) if arg_count else 0
        }
    return macros


def verbFun(grd: str) -> list:
    '''Computes the line positions of the verbatim'''
    beg, en, BE = [], [], 'start'

    for i, el in enumerate(grd):
        if re.match(r'^((\\%)|[^%])*\\begin{verbatim}', el):
            beg.append(i)
            BE = 'begin'
        if el.find('\\end{verbatim}') != -1 and BE == 'begin':
            en.append(i)
            BE = 'end'

    if len(beg) != len(en):
        sys.exit('latex error found in tex file')

    return [list(el) for el in zip(beg, en)]


def verbTest(i: str, verbList: list) -> bool:
    '''test if line is in verbatim block'''
    for el in verbList:
        if el[1] >= i >= el[0]:
            return False
    return True


def eqFun(grd: str, tipo: str) -> list:
    '''Computes the line positions of equations'''
    if tipo not in eqList:
        sys.exit('tipo must be either equation, align or eqnarray')

    beg, en, test, verb = [], [], 'close', verbFun(grd)

    for i, el in enumerate(grd):
        if re.match(r'^((\\%)|[^%])*\\begin{' + tipo + '}', el) and verbTest(
                i, verb):
            if test == 'open':
                sys.exit('latex error found in tex file')
            beg.append(i)
            test = 'open'
        if re.match(r'^((\\%)|[^%])*\\end{' + tipo + '}', el) and verbTest(
                i, verb):
            if test == 'close':
                sys.exit('latex error found in tex file')
            en.append(i)
            test = 'close'

    if len(beg) != len(en):
        sys.exit('latex error found in tex file')

    return [list(el) for el in zip(beg, en)]


def submit(nam: str,
           fold: str = 'submit',
           newNam: str = '',
           com: str = 'yes',
           space: str = 'all',
           dobRet: str = 'yes',
           figs: str = '',
           eqCheck: str = 'no',
           *wargs) -> None:
    '''main function to prepare submission'''

    if not pathlib.Path(nam + '.tex').is_file():
        sys.exit(nam + '.tex not found, aborting')

    if dobRet not in ['yes', 'no']:
        sys.exit('dobRet must be either yes or no')

    if eqCheck not in ['yes', 'no']:
        sys.exit('eqCheck must be either yes or no')

    if com not in ['yes', 'no']:
        sys.exit('com must be either yes or no')

    if space not in ['left', 'right', 'trim', 'all']:
        sys.exit('space must be either left, right, trim or all')

    if newNam == '':
        newNam = nam

    path = os.getcwd() + '/' + fold + '/'

    if os.path.isdir(path):
        sys.exit('directory exists already, aborting')

    if figs != '':
        figs = figs + '/'

    os.mkdir(path)

    with open(nam + '.tex') as fp:
        grid = []
        for el in fp.read().splitlines():
            grid.append(el)
            if re.match(r'^((\\%)|[^%])*\\end{document}', el):
                break
        verb, gridClean = verbFun(grid), []
        for i, el in enumerate(grid[:]):
            if not (re.match(r'^\s*%', el) and verbTest(i, verb)):
                gridClean.append(re.sub(r'(^.*?(?<!\\)(\\\\)*)%.+$', '\\1', el).strip())
        if com == 'yes':
            grid = gridClean[:]
            verb = verbFun(grid)

    for el in verb[::-1]:
        gridClean[el[0]:el[1] + 1] = []

    if space in ['trim', 'all']:
        for i in range(len(grid)):
            if verbTest(i, verb):
                grid[i] = grid[i].strip()
    elif space == 'left':
        for i in range(len(grid)):
            if verbTest(i, verb):
                grid[i] = grid[i].lstrip()
    elif space == 'right':
        for i in range(len(grid)):
            if verbTest(i, verb):
                grid[i] = grid[i].rstrip()
                
    macros = find_macros(''.join(gridClean))

    figurin = []

    for macro, details in macros.items():
        kkk = re.findall(r'\\includegraphics', ''.join(details['definition']))
        if len(kkk) == 1:
            figurin.append(macro)

    verb = verbFun(grid)
    
    figList = re.findall(r'\\includegraphics.*?{([^{]*?)}', ''.join(gridClean)) \
    + re.findall(r'\\epsfig{file\s*=\s*([^\s,]*\s*),', ''.join(gridClean))
    
    for el in figurin:
            figList = figList + re.findall(r'\\' + el + '.*?{([^{]*?)}', ''.join(gridClean))

    for i, el in enumerate(grid[:]):
        if el.strip() == '':
            grid[i] = ''
        if verbTest(i, verb) and space == 'all':
            grid[i] = re.sub(r'\s+', ' ', el)

    if dobRet == 'yes':
        grid2 = [grid[0]]
        for i, el in enumerate(grid[1:], start=1):
            if not (el == '' and el == grid[i - 1] and verbTest(i, verb)):
                grid2.append(el)
        grid = grid2

    verb = verbFun(grid)
    
    fg = []
    
    for el in figList:
        j = el.rfind('/')
        if j != -1:
            fg.append(el[:j + 1])
            
    fg = sorted(list(dict.fromkeys(fg)), key = len)[::-1]

    for el in fg:
        for i in range(len(grid)):
            if verbTest(i, verb):
                grid[i] = grid[i].replace(el, figs)

    if grid[0] == '':
        del (grid[0])

    if grid[-1] == '':
        del (grid[-1])

    if eqCheck == 'yes':
        posEq = []
        for el in eqList:
            posEq += eqFun(grid, el)
        posEq.sort()

        for el in posEq[::-1]:
            for i in range(*el):
                if grid[i].strip() == '':
                    del (grid[i])
                    
    verb = [-1] + list(np.array(verbFun(grid)).flatten()) + [len(grid) + 1]
    verbGrid = []
    
    for i, el in enumerate(verb[:-1]):
        if i%2 == 0:
            verbGrid.append(re.sub(r'{ *\n *', '{', re.sub(r' *\n *}', '}',
                            '\n'.join(grid[el + 1: verb[i + 1]]))))
        else:
            verbGrid.append('\n'.join(grid[el: verb[i + 1] + 1]))

    grid = [
        '%% This file was processed using pySubmit, script created' +
        ' by Vicent Mateu\n'
    ] + verbGrid

    grid = '\n'.join(grid)

    print(grid, file=open(path + newNam + '.tex', 'w'))

    if pathlib.Path(nam + '.bbl').is_file():
        copy2(nam + '.bbl', path + newNam + '.bbl')
    else:
        print(nam + '.bbl not found. Continuing without it')

    for el in wargs:
        if pathlib.Path(el).is_file():
            copy2(el, path)
        else:
            print(el + ' not found. Continuing without it')

    pathfig = path

    if figs != '':
        os.mkdir(path + figs)
        pathfig = path + figs

    for i, el in enumerate(figList[:]):
        if not pathlib.Path(el).is_file():
            for em in formatList:
                if pathlib.Path(el + em).is_file():
                    figList[i] = el + em
                    break
        if pathlib.Path(os.getcwd() + '/' + figList[i]).is_file():
            copy2(os.getcwd() + '/' + figList[i], pathfig)
        else:
            print('Figure' + figList[i] + ' not found. Continuing without it')

    os.chdir(path)
    tar = tarfile.open(newNam + '.tar.gz', 'w:gz')
    tar.add(newNam + '.tex')
    if pathlib.Path(newNam + '.bbl').is_file():
        tar.add(newNam + '.bbl')
    for el in wargs:
        if pathlib.Path(el).is_file():
            tar.add(el)
    if figs == '':
        for el in figList:
            if pathlib.Path(el[el.find('/') + 1:]).is_file():
                tar.add(el[el.find('/') + 1:])
    else:
        tar.add(figs)
    tar.close()


def main(argv=None):
    '''Main method to deal with command line arguments'''
    if not argv:
        argv = sys.argv

    parser = argparse.ArgumentParser(
        prog='pySubmit.py',
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(action='store',
                        type=str,
                        dest='nam',
                        help='name of the tex file, w/o the .tex extension')

    parser.add_argument(
        '-d',
        '--submit-directory',
        action='store',
        type=str,
        dest='fold',
        default='submit',
        help=
        'name of the directory in which all files are copied and the .tar.gz is created'
    )

    parser.add_argument(
        '-f',
        '--figures-directory',
        action='store',
        type=str,
        dest='fig',
        default='',
        help='name of the directory in which all figures are copied')

    parser.add_argument(
        '-nn',
        '--new-name',
        action='store',
        type=str,
        default='',
        dest='newNam',
        help='a new name for the tex and bbl file w/o their extensions')

    parser.add_argument(
        '-sp',
        '--space',
        dest='space',
        type=str,
        help='how to remove white spaces: left, right, trim or all',
        default='all')

    parser.add_argument('-cr',
                        '--carriage-return',
                        dest='dobRet',
                        type=str,
                        help='remove double carriagde returns: yes or no',
                        default='yes')

    parser.add_argument(
        '-eq',
        '--check-equations',
        dest='eq',
        type=str,
        help='check for empty lines within equations: yes or no',
        default='no')

    parser.add_argument('-c',
                        '--comments',
                        dest='com',
                        type=str,
                        help='remove comments: yes or no',
                        default='yes')

    parser.add_argument(
        '-add',
        '--additional-files',
        nargs='+',
        dest='addFiles',
        default=[],
        help='additional files to copy besides figures, bbl and tex files')

    options = parser.parse_args()

    if options.newNam == '':
        options.newNam = options.nam

    fit = submit(options.nam, options.fold, options.newNam, options.com,
                 options.space, options.dobRet, options.fig, options.eq,
                 *options.addFiles)


if __name__ == "__main__":
    sys.exit(main(sys.argv))
