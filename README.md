pySubmit.py - python3 script to prepare arXiv or journal submissions

Autor: Vicent Mateu

The script creates a new copy of the tex file, removing by default all comments. It will also remove all text after the \end{document} statement. If specified, by the user it will also delete a) double whitespaces, trailing whitespaces or indentation, and b) double carriage returns. It will try to copy figures in pdf format, but if not found, it will look for the following formats (in this order): eps, png, jpg, jpeg, jbig2, jp2. The script creates a user-specified directory and by default places all figures in the same directory as the tex and bbl files (although this can be changed), plus a number of other user-specified files, while the tex file is modified accordingly (this is necessary to submit e.g. to APS journals). All files are then compressed into a tar.gz, ready for submission to arXiv or any journal. By default the script prepares the submission files to be compiled with pdflatex.

Assuming pySubmit.py has been made exectuble with the command

`chmod +x pySubmit.py`

one can execute the script as follows

1) Help

`./pySubmit.py -h`

or

`./pySubmit.py --help`

2) Usage:

`./pySubmit.py name [-d directory] [-nn NewName] [-sp all] [-cr yes] [-add jheppub.sty JHEP.bst]`

positional argument:
* name:                  name of the tex file, without the .tex extension

optional arguments:
* -h, --help:            show help message and exit
* -d, --submit-directory: 
                        name of the directory in which all files are copied and the .tar.gz is created
                        (default: submit)
* -nn, --new-name: 
                        a new name for the tex and bbl file w/o their extensions (default: name)
*  -c, --comments:
                        remove comments. Possible values [yes, no] (default: yes)
*  -sp, --space: 
                        how to remove white spaces [left, right, trim, all] (default: all)
*  -cr, --carriage-return: 
                        remove double carriagde returns [yes, no] (default: yes)
* -f, --figures-directory:
                        name of the directory in which all figures are copied (default: root directory)
* -eq, --check-equations:
                        check for empty lines within equations [yes, no] (default: no)
*  -add, --additional-files: 
                        additional files to copy besides figures, bbl and tex files (default: None)

Under windows one should use

`py pySubmit.py name [-d directory] [-nn NewName] [-sp all] [-cr yes] [-add jheppub.sty JHEP.bst]`

Under linux/macOS one can also use

`python3 pySubmit.py name [-d directory] [-nn NewName] [-sp all] [-cr yes] [-add jheppub.sty JHEP.bst]`

<!---* draftName: name of the tex file, without the .tex extension
* submitFolder: (optional) name of the directory in which all files are copied and the .tar.gz is created
* NewName: (optional)a new name for the tex and bbl file w/o their extensions
* -add: (optional) files to copy besides figures, bbl and tex files (e.g. journal style files)

The order of the flagged arguments is irrelevant, but must be preceded by the right flag-->
